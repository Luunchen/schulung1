import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java01 {
	/**
	 * Expressions used in Statements to initialize variables of primitive type.
	 * - Variablen mit primitiven Datentypen
	 * - Literale (Konstanten)
	 * - Operatoren `=`, `+` fuer `int` Werte
	 * - Testen der Variablen Inhalte
	 */
	@Test
	void test01() {
		/**
		 * ℤ Ganzzahlen -> Integer Variable, primitiver datentyp `int`
		 * - Hier mit Variablenname `i`
		 * - Hier besetzt die `int` Variable 32-bit oder 4 Byte Speicher
		 *
		 * Genutzte Literale (Konstante)
		 * - `1` Wert 1 als `int`
		 * - `2` Wert 2 als `int`
		 * 
		 * Genutzte Operatoren
		 * - `=` wert-zuweisung, assignment
		 * - `+` addition
		 */
		int i = 1; // ℤ ganzzahl, variable (veraenderbar, mutable), initialisiert mit dem Wert `1` (literal)
		i = i + 1; // Erhoehe den Inhalt der Variable `i` um `1` (literal).
		Assertions.assertEquals(2, i); // Test das variable `i` den Inhalt `2` (literal) hat.
		
		/**
		 * ℤ Ganzzahlen -> Integer Variable, primitiver datentyp `int`
		 * - Hier mit Variablenname `j`
		 * - ...
		 */
		int j; // Undefiniert, nicht initialisiert.
		j = 1; // Weise der Variable den Wert `1` (literal) zu.
		j = j + 1; // Erhoehe den Wert der Variable `j` um `1` (literal)
		Assertions.assertEquals(2, j); // Test das variable `j` den Inhalt `2` (literal) hat.
	}
	
	/** 
	 * Primitive Datentypen und Literale (Konstante)
	 */
	@Test
	void test02_literals() {
		boolean b0 = false; // boolean-value literal
		byte o0 = 0b0010; 	// bit-value literal -> decimal 2 
		char c0 = 'C';    	// UTF-16 character value literal
		short s0 = 0x0A;  	// hex-value literal -> decimal 10
		int i0 = 100;     	// int-value literal (default)
		long l0 = 1000L;  	// long-value literal
		float f0 = 3.14f; 	// float-value literal
		double d0 = 2.14; 	// double--value literal (default)
		Assertions.assertEquals(false, b0);
		Assertions.assertEquals((byte)2, o0);
		Assertions.assertEquals('C', c0);
		Assertions.assertEquals(10, s0);
		Assertions.assertEquals(100, i0);
		Assertions.assertEquals(1000, l0);
		Assertions.assertEquals(3.14f, f0, Float.MIN_VALUE); // Test value mit erlaubter Fehler-Toleranz Float.MIN_VALUE
		Assertions.assertEquals(2.14, d0, Double.MIN_VALUE); // Test value mit erlaubter Fehler-Toleranz Double.MIN_VALUE		
	}
	
	/** 
	 * Primitive Datentypen und Literale (Konstante)
	 */
	@Test
	void test03_immutable() {
		// Unveraenderbare (Immutable) Variablen, i.e. Konstante
		final int const_i0 = 1;
		// const_i0 = const_i0 + 1; // FEHLER
		Assertions.assertEquals(1, const_i0);		
	}
	
	/** 
	 * Block-Statements und Lebensbereich lokaler Variablen
	 */
	@Test
	void test04_block() { // Block-0 Anfang
		// Block-1 Anfang
		{ 
		    int i = 1; // Neue variable `i` mit dem Wert `1` initialisiert
		    Assertions.assertEquals(1, i);
		    
		    // Block-2 Anfang
		    {
		        int j = 2; // Neue variable `j` mit dem Wert `2` initialisiert
		        int k = i + j; // Neue variable `k` mit `i+j` initialisiert
		        Assertions.assertEquals(3, k);
		    } 
		    // Block-2 Ende
		    // Variablen `j` und `k` existieren nicht mehr!
		}
		// Block-1 Ende
		// Variable `i` existiert nicht mehr!
		// i = i + 1; FEHLER, `i` gibts nicht mehr
		
		// Neue verschachtelte Bloecke mit neuen Variablen & Lebensbereich
		{
			int i = 2;
			{
				int j = 3;
				int k = i + j;
				Assertions.assertEquals(5, k);
			}
		}
	} // Block-0 Ende
	
	/** 
	 * Binary-Operatoren der 4 Grundrechenarten und Modulo (Divisionsrest) anhand des primitiven Datentyps `int`
	 */
	@Test
	void test10_grundrechenarten() {
		// Addition
		{
			{
				Assertions.assertEquals(3, 1+2);
			}
			{
				int i = 6; // positiver Wert
				int j = 2; // positiver Wert
				int k = i + j;
				Assertions.assertEquals(8, k);
			}
			{
				int i = +6; // positiver Wert
				int j = -2; // negativer Wert!!
				int k = i + j;
				Assertions.assertEquals(4, k);
			}
		}
		// Subtraktion
		{
			int i = 6;
			int j = 2;
			int k = i - j;
			Assertions.assertEquals(4, k);
		}
		// Multiplikation
		{
			int i = 6;
			int j = 2;
			int k = i * j;
			Assertions.assertEquals(12, k);
		}
		// Division
		{
			int i = 6;
			int j = 2;
			int k = i / j;
			Assertions.assertEquals(3, k);
		}
		// Modulo (Divisionsrest)
		{
			{
				int i = 6;
				int j = 2;
				int k = i % j;
				int l = i - ( i / j ) * j; // Modulo definition, i.e. Divisionsrest
				Assertions.assertEquals(l, k);
				Assertions.assertEquals(0, k);
			}
			{
				int i = 7;
				int j = 2;
				int k = i % j;
				int l = i - ( i / j ) * j; // Modulo definition, i.e. Divisionsrest
				Assertions.assertEquals(l, k);
				Assertions.assertEquals(1, k);
			}
		}
	}
	
	/** 
	 * Unary-Operatoren (1 Argument) anhand des primitiven Datentyps `int`
	 */
	@Test
	void test11_unary_post_prefix() {
		// Prefix erhoehe (increment) und veringere (decrement)
		{
			int i = 6;
			++i;
			Assertions.assertEquals(7, i);
			--i;
			Assertions.assertEquals(6, i);
			
			// !!!!
			Assertions.assertEquals(6, i);   // Inhalt von `i` ist `6`
			Assertions.assertEquals(7, ++i); // Inhalt von `i` wird erhoeht, dann zurueckgegeben!
			Assertions.assertEquals(7, i);   // Selber Wert
		}
		
		// Postfix erhoehe (increment) und veringere (decrement)
		{
			int i = 6;
			i++;
			Assertions.assertEquals(7, i);
			i--;
			Assertions.assertEquals(6, i);
		
			// !!!!
			Assertions.assertEquals(6, i);   // Inhalt von `i` ist `6`
			Assertions.assertEquals(6, i++); // Inhalt von `i` wird zurueckgegeben, dann erhoeht! 
			Assertions.assertEquals(7, i);   // Nun ist der Inhalt von `i`erhoeht 
		}		
	}
	
	/** 
	 * Zuweisungs-Operatoren inklusive der 4 Grundrechenarten anhand des primitiven Datentyps `int`
	 */
	@Test
	void test12_zuweisung() {
		// Einfache Zuweisung
		{
			int i = 6;
			Assertions.assertEquals(6, i);
			i = 7;
			Assertions.assertEquals(7, i);			
		}
		
		// Addition-Zuweisung
		{
			int i = 6;
			i += 4;
			Assertions.assertEquals(10, i);
		}
		// Subtraktion-Zuweisung
		{
			int i = 6;
			i -= 4;
			Assertions.assertEquals(2, i);
		}
		// Multiplikation-Zuweisung
		{
			int i = 6;
			i *= 4;
			Assertions.assertEquals(24, i);
		}
		// Division-Zuweisung
		{
			int i = 6;
			i /= 2;
			Assertions.assertEquals(3, i);
		}
		// Modulo-Zuweisung
		{
			{
				int i = 6;
				i %= 2;
				Assertions.assertEquals(0, i);
			}
			{
				int i = 7;
				i %= 2;
				Assertions.assertEquals(1, i);
			}
		}
	}
	/** 
	 * Operatoren der logischen Vergleiche anhand des primitiven Datentyps `int`
	 */
	@Test
	void test13_vergleich() {
		// Gleichheit (equality)
		{
			int i = 8;
			int j = 8;
			int k = 9;
			Assertions.assertEquals(true,  i == j);
			Assertions.assertEquals(false, i != j);
			
			Assertions.assertEquals(false, i == k);
			Assertions.assertEquals(true,  i != k);
		}
		// Relational
		{
			int i = 8;
			int j = 8;
			int k = 9;
			Assertions.assertEquals(false, i <  j);
			Assertions.assertEquals(true,  i <= j);
			Assertions.assertEquals(true,  i >= j);
			Assertions.assertEquals(false, i >  k);
			
			Assertions.assertEquals(true,  i <  k);
			Assertions.assertEquals(true,  i <= k);
			Assertions.assertEquals(false, i >= k);
			Assertions.assertEquals(false, i >  k);			
		}
	}
	
	/** 
	 * Operatoren der logischen Verknuepfung anhand des primitiven Datentyps `int` und `boolean`
	 */
	@Test
	void test14_logisch() {
		// Logisch-Und (and) als auch Logisch-Oder (or)
		{
			int i = 8;
			int j = 8;
			int k = 9;
			boolean b0 = i==j;
			boolean b1 = i==k;
			
			Assertions.assertEquals(true,   b0 && !b1);
			Assertions.assertEquals(false, !b0 ||  b1);
			
			Assertions.assertEquals(true,  i == j && i != k);
			Assertions.assertEquals(false, i != j || i == k);			
		}
	}
	
	/** 
	 * Programmfluss-Statement: Branches (if, switch-case, conditional-op)
	 */
	@Test
	void test20_branch() {
	    // branches: if
	    {
	    	int state = -1;
	        int a = 0;

	        // Note that we place the immutable literal (r-value) on the left-side
	        // of the equality operation '0 == a',
	        // which avoids accidental assignment of a mutable l-value if typo 'a = 0'.
	        if( 0 == a ) {
	            // executed if 'a' contains '0'
	            state = 1;
	            Assertions.assertTrue(true);
	        } else if( 1 == a ) {
	            // executed if 'a' contains '1'
	        	Assertions.assertTrue(false); // unreachable
	        }
	        
	        Assertions.assertEquals(1, state);
	        state = -1;

	        // Note that the expression `0 == a` is a boolean expression,
	        // i.e. resolved to either `true` or `false`.
	        final boolean b0 = 0 == a;
	        final boolean b1 = 1 == a;

	        // Note that the expression for `if` and `while` are boolean expressions.
	        //
	        // Below we use the pre-computed boolean results.
	        if( b0 ) {
	            // executed if b0 is true, i.e. 'a' contains '0'
	        	state = 1;
	        	Assertions.assertTrue(true);
	        } else if( b1 ) {
	            // executed if b1 is true, i.e. 'a' contains '1'
	        	Assertions.assertTrue(false); // unreachable
	        }
	        
	        Assertions.assertEquals(1, state);
	    }

	    // branches: switch
	    {
	    	int state = -1;
	        int a = 0;

	        switch( a ) {
	            case 0:
	                // executed if 'a' contains '0'
	            	state = 1;
	            	Assertions.assertTrue(true);
	                break; // ends code for this case
	            case 1:
	                // executed if 'a' contains '0'
	                {
	                    // use an inner block-statement to allow local case resources
	                    int v = 1;
	                    System.out.println("branch1."+v);
	                }
	                Assertions.assertTrue(false); // unreachable
	                break; // ends code for this case
	            case 2:
	                // executed if 'a' contains '2'
	                // and falls through to default case code
	                // [[fallthrough]];
	            	Assertions.assertTrue(false); // unreachable
	            default:
	                // executed if none of the above cases matches
	            	Assertions.assertTrue(false); // unreachable
	                break; // ends code for this case
	        }
	        Assertions.assertEquals(1, state);
	    }

	    // branches: conditional operator
	    {
	        int a = 0;

	        // initialized with '0' if 'a' contains '0', otherwise initialized with '1'
	        char c = ( 0 == a ) ? '0' : '1';

	        Assertions.assertEquals('0', c);
	    }		
	}
	
	/** 
	 * Programmfluss-Statement: Loops (while, do-while, for, break)
	 */
	@Test
	void test21_loops() {
	    final int loop_count = 3;
	    // Same loop as while
	    {
	        // while loop, an exploded for-loop (see below)
	    	int v=10;
	        int i=0;              /* instantiation and initialization of loop variable */
	        while( i < loop_count /* while condition */ ) {
	            ++v;
	            i = i + 1;        /* tail expression */
	        }
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Same loop as do-while
	    {
	        // do-while loop - executed at least once
	    	int v=10;
	        int i=0;              /* instantiation and initialization of loop variable */
	        do {
	            ++v;
	            i = i + 1;        /* tail expression */
	        } while( i < loop_count /* while condition */ );
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Same loop as for (1)
	    {
	    	int v=10;
	        int i;                /* instantiation of loop variable*/
	        for(i=0               /* initialization of loop variable*/;
	            i<loop_count      /* while condition */;
	            ++i               /* tail expression */)
	        {
	        	++v;
	        }
	        // `i` is still in scope!
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Same loop as for (2)
	    {
	    	int v=10;
	        /* instantiation and initialization of loop variable; while condition; tail expression */
	        for(int i=0; i<loop_count; ++i) {
	            ++v;
	        }
	        // `i` is out of scope!
	        // Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Using break within a loop
	    {
	    	int v=10;
	        int i=0;              /* instantiation and initialization of loop variable */
	        while( true /* while condition: forever */ ) {
	            if( i >= loop_count ) {
	                break; // exit loop
	            }
	        	++v;
	            i = i + 1;        /* tail expression */
	        }
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }		
	}
	
	static int addiere(int a, int b) {
		return a+b;
	}
	
	void test30_method_static() {
		Assertions.assertEquals(5, addiere(2, 3));
	}
	
	int instanz_var = 0;
	
	int erhoeheUm(int a) {
		instanz_var += a;
		return instanz_var;
	}
	
	void test31_method_instanz() {
		Assertions.assertEquals(2, erhoeheUm(2));
		Assertions.assertEquals(2, instanz_var);
		Assertions.assertEquals(5, erhoeheUm(3));
		Assertions.assertEquals(5, instanz_var);
	}
	
}
