import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java01Aufgabe01 {
	/**
	 * TODO: Gebe die Summe der ersten `n` integer werte 
	 * beginnend von `0` zurueck.
	 * <pre> 
	 * Z.B.
	 * - n==0 -> { 0 }       -> 0     -> return 0  
	 * - n==1 -> { 0, 1 }    -> 0+1   -> return 1   
	 * - n==2 -> { 0, 1, 2 } -> 0+1+2 -> return 3  
	 * </pre> 
	 */
	static int summe_a(int n) {
		return n; // FIXME FALSCH
	}
	
	/**
	 * Test fuer {@link #summe_a(int)}. 
	 */
	@Test
	void test01_summe() {
		Assertions.assertEquals( 0, summe_a(0));
		Assertions.assertEquals( 1, summe_a(1));
		Assertions.assertEquals( 3, summe_a(2));
		Assertions.assertEquals( 6, summe_a(3));
		Assertions.assertEquals(28, summe_a(7));
	}
	
	/**
	 * TODO: Gebe die Summe der integer werte von [`a`..`b`] zurueck, 
	 * <pre> 
	 * Z.B. 
	 * - a==0, b==2 -> { 0, 1, 2 } -> 0+1+2 -> return 3  
	 * - a==2, b==4 -> { 2, 3, 4 } -> 2+3+4 -> return 9  
	 * </pre> 
	 */
	static int summe_b(int a, int b) {
		return a+b; // FIXME FALSCH
	}
	
	/**
	 * Test fuer {@link #summe_a(int)}. 
	 */
	@Test
	void test02_summe() {
		Assertions.assertEquals( 0, summe_b(0, 0));
		Assertions.assertEquals( 1, summe_b(0, 1));
		Assertions.assertEquals( 3, summe_b(0, 2));
		Assertions.assertEquals( 6, summe_b(0, 3));
		Assertions.assertEquals(28, summe_b(0, 7));
		
		Assertions.assertEquals( 9, summe_b(2, 4));
	}
	
	/**
	 * TODO: Gebe das Produkt der integer werte von `a` * `b` zurueck, 
	 * _OHNE_ die eingebaute multiplikation zu verwenden, d.h. nur mit addition.
	 * <pre> 
	 * Z.B. 
	 * - a==2, b==3 -> 2 * 3 -> return 6
	 * </pre> 
	 */
	static int mul_a(int a, int b) {
		return a+b; // FIXME FALSCH
	}
	
	/**
	 * Test fuer {@link #summe_a(int)}. 
	 */
	@Test
	void test03_mul() {
		Assertions.assertEquals( 0, mul_a(0, 0));
		Assertions.assertEquals( 1, mul_a(1, 1));
		Assertions.assertEquals( 2, mul_a(1, 2));
		Assertions.assertEquals( 6, mul_a(2, 3));
		Assertions.assertEquals(28, mul_a(4, 7));
		
		Assertions.assertEquals( 9, mul_a(3, 3));
	}
	
}
